<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <title>Купить Omlook X5 PRO</title>
    <link rel="stylesheet" href="/css/main.css" type="text/css">
    <link rel="stylesheet" href="/css/menu.css" type="text/css">

    <!-- start jquery tabs -->
    <link rel="stylesheet" href="/css/tabs.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#tabs" ).tabs();
        });
    </script>
    <!-- end jquery tabs -->

    <!-- vk comment start -->
    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>

    <script type="text/javascript">
        VK.init({apiId: 5103481, onlyWidgets: true});
    </script>
    <!-- vk comment end -->

</head>

<body>

<div id="content-pay">




    <!-- start menu block -->
    <!--       <div id="menu-block">
               <center>
                   <ul class="menuTemplate3 decor3_1" license="mylicense">
                       <li><a href="#CSS-Menu">Дизайн</a></li>
                       <li><a href="#CSS">Параметры</a></li>
                       <li><a href="#Horizontal-Menu-CSS">Возможности</a></li>
                       <li><a href="#Horizontal-Menu-CSS">Гарантия</a></li>
                       <li><a href="#Horizontal-Menu-CSS">Обмен</a></li>
                       <li><a href="#Horizontal-Menu-CSS">Доставка</a></li>
                       <li><a href="#Horizontal-Menu-CSS">Заказать</a></li>
                       <li><a href="">Есть вопросы? 8 (800) 700 43 43</a></li>
                   </ul>
               </center>
           </div>  -->
    <!-- end menu block -->

    <!-- start content-payment block -->
    <div id="content-payment">
        <center>
            <h1 style="color: #002DFF; padding-top: 20px; font-weight: 100;">Заказ и оплата: <span>Omlook X5 PRO</span> </h1>
        </center>
        <hr>
        <p>
        <div id="zakaz">
            <?php
            //Проверяем, была ли форма отправлена
            if(isset($_POST['submit'])){

                //Создаем массив, в который будем складывать ошибки
                $errors = array();

                //Проверяем указал ли пользователь имя
                if(empty($_POST['CHname'])){
                $errors[] = "Пожалуйста, введите ваше ФИО";
            }else{
                    $name = strip_tags($_POST['CHname']);
                }

                //Проверяем указал ли пользователь область
                if(empty($_POST['state'])){
                    $errors[] = "Пожалуйста, введите вашу область";
                }else{
                    $name = strip_tags($_POST['state']);
                }

                //Проверяем указал ли пользователь город
                if(empty($_POST['city'])){
                    $errors[] = "Пожалуйста, введите ваш город";
                }else{
                    $name = strip_tags($_POST['city']);
                }

                //Проверяем указал ли пользователь улицу
                if(empty($_POST['street'])){
                    $errors[] = "Пожалуйста, введите ваш улицу";
                }else{
                    $name = strip_tags($_POST['street']);
                }

                //Проверяем указал ли пользователь телефон
                if(empty($_POST['phone'])){
                    $errors[] = "Пожалуйста, введите ваш телефон";
                }else{
                    $name = strip_tags($_POST['phone']);
                }

                //Проверяем email адрес на корректность
                if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                    $errors[] = "Пожалуйста, введите корректный email адрес";
                }else{
                    $email = $_POST['email'];
                }

                //Обрабатываем поле комментариев
                if(empty($_POST['zip'])){
                    $errors[] = "Пожалуйста, введите ваш индекс";
                }else{
                    $comment = strip_tags($_POST['zip']);
                }

                //Обрабатываем поле комментариев
                if(empty($_POST['agrement'])){
                    $errors[] = "Пожалуйста, прочитайте и согласитесь с условиями использования сайта";
                }else{
                    $comment = strip_tags($_POST['agrement']);
                }
            }



            ?>
            <?php if(isset($errors)){ ?>
                <ul id="error-list">
                    <?php foreach($errors as $msg){
                        echo "<li>".$msg."</li>";
                    }?>
                </ul>
            <?php } ?>
            <form method="post" action="gate.php">
                <table>
                    <tr>
                        <td>Цвет телефона:</td>
                        <td>
                            <select name="country" size ="1">
                                <option value=white>Белый</option>
                                <option value=black>Чёрный</option>
                                <option value=red>Красный</option>
                                <option value=green>Зелёный</option>
                                <option value=blue>Синий</option>
                                <option value=yelow>Жёлтый</option>
                                <option value=pink>Розовый</option>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>ФИО:</td>
                        <td><input type="text" name="CHname" size="30" maxlength="80" minlength="5" placeholder="ФИО получателя"></td>
                    </tr>
                    <tr>
                        <td>Ваша страна:</td>
                        <td><select name="country" size ="1" autofocus required>
                                <option value=RU>Россия</option>
                                <option value=UA>Украина</option>
                                <option value=BY>Беларусь</option>
                                <option value=KZ>Казахстан</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>Край/Область/Регион:</td>
                        <td><input type="text" name="state" size="30" maxlength="40" minlength="5"></td>
                    </tr>
                    <tr>
                        <td>Город:</td>
                        <td><input type="text" name="city" size="30" maxlength="20" minlength="2" placeholder="Город доставки"></td>
                    </tr>
                    <tr>
                        <td>Адрес:</td>
                        <td><input type="text" name="street" size="30" maxlength="20" minlength="2" placeholder="Улица, дом, квартира"></td>
                    </tr>
                    <tr>
                        <td>Индекс:</td>
                        <td><input type="text" name="zip" size="30" maxlength="20" minlength="2" placeholder="Почтовый индекс" ></td>
                    </tr>
                    <tr>
                        <td>Ваш e-mail:</td>
                        <td><input type="email" name="email" size="30" maxlength="71" minlength="3"  placeholder="ваш@адрес"></td>
                    </tr>
                    <tr>
                        <td>Ваш номер телефона:</td>
                        <td><input type="text" name="phone" size="30" maxlength="20" minlength="5" placeholder="79991234567"></td>
                    </tr>
                    <tr>
                        <td>Ваш логотип (<a href="/Individual inscription on the phone.pdf" target="_blank">подробнее</a>):</td>
                        <td><input type="file" name="logo"></td>
                    </tr>

                </table>

                <p style="font-size: 14px; padding: 2px; margin: 0px;">
                    <input type="checkbox" name="agrement" style="width: 10px; height: 10px">
                    Я прочитал и соглашаюсь с
                    <a href="/Site rules.pdf" target="_blank">правилами и условиями использования сайта</a>
                </p>

                <input type="submit" name="submit" value="Продолжить" style="margin-left: 350px; width: 120px; height: auto; background-color: #00CC66; color: white; font-size: 16px;" ><br>

            </form>


        </div>
        <div id="info">

            <p style="font-size: 14px;">
                <strong>Информация о товаре:</strong><br><br>
                <img src="/img/om1.jpg" style="max-height: 200px"><br><br>
                Телефон Omlook X5 PRO<br>
                Срок доставки: 15-30 дней<br>
                Стоимость доставки: бесплатно
            </p>
        </div>
        <div id="itogo">
            <hr>
            <span class="allpayment">Итого к оплате: 6999 рублей</span>

        </div>
        <br>

    </div>
    <!-- end content-payment block -->
    <!-- start fotter wrap -->

    <!-- start fotter block -->
    <div id="fotter">
        <table cellpadding="15px">
            <tr>
                <td>
                    <img src="/img/visamc.gif" style="max-height: 60px">
                </td>
                <td>
                    <h5>
                        Omlook Ltd., Office 11, 43 Bedford street, Covent Garden  London UK<br>
                        Регистрационный номер 7647188<br>
                        тел.: +7 495 135 06 82; e-mail: w@omlook.com<br>
                        <a href="/Site rules.pdf" target="_blank">Правила и условия использования сайта</a> |
                        <a href="/Privacy policy.pdf" target="_blank">Политика конфиденциальности</a> |
                        <a href="/Exchange and Returns.pdf" target="_blank">Обмен и возврат</a>

                    </h5>
            </tr>
        </table>
    </div>
    <!-- end fotter block -->

</div>
<!-- end container block -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'bogZ8Xhqfy';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

</body>

</html>