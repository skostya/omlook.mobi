<?php
$msg_box = ""; // в этой переменной будем хранить сообщения формы

if($_POST['btn_submit']){
    $errors = array(); // контейнер для ошибок
    // проверяем корректность полей
    if($_POST['user_telephone1'] == "") $errors[] = "Поле 'Контактный телефон' не заполнено!";

    // если форма без ошибок
    if(empty($errors)){
        // собираем данные из формы
        $message = "Телефон клиента: " . $_POST['user_telephone1'];
        send_mail($message); // отправим письмо
        // выведем сообщение об успехе
        $msg_box = "<span style='color: green;'>Спасибо! Наш менеджер скоро свяжется с вами. </span>";
    }else{
        // если были ошибки, то выводим их
        $msg_box = "";
        foreach($errors as $one_error){
            $msg_box .= "<span style='color: red;'>$one_error</span><br/>";
        }
    }
}

// функция отправки письма
function send_mail($message){
    // почта, на которую придет письмо
    $mail_to = "k.shelest@omlook.com";
    // тема письма
    $subject = "Omlook mobile callbackbuy form";

    // заголовок письма
    $headers= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
    $headers .= "From: Omlook mobile <no-reply@omlook.mobi>\r\n"; // от кого письмо

    // отправляем письмо
    mail($mail_to, $subject, $message, $headers);
}

header("Location: http://omlook.mobi/#openModal7");
?>
