<html xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <title>Omlook X5 PRO</title>
    <link rel="stylesheet" href="/css/main.css" type="text/css">
    <link rel="stylesheet" href="/css/menu.css" type="text/css">

    <!-- start jquery tabs -->
    <link rel="stylesheet" href="/css/tabs.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#tabs" ).tabs();
        });
    </script>
    <!-- end jquery tabs -->

    <!-- vk comment start -->
    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>

    <script type="text/javascript">
        VK.init({apiId: 5103481, onlyWidgets: true});
    </script>
    <!-- vk comment end -->

</head>

<body>

<!-- start fb-comment block -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!-- end fb-comment block -->

<!-- start container block -->
<div id="container">

        <!-- start menu block -->
 <!--       <div id="menu-block">
            <center>
                <ul class="menuTemplate3 decor3_1" license="mylicense">
                    <li><a href="#CSS-Menu">Дизайн</a></li>
                    <li><a href="#CSS">Параметры</a></li>
                    <li><a href="#Horizontal-Menu-CSS">Возможности</a></li>
                    <li><a href="#Horizontal-Menu-CSS">Гарантия</a></li>
                    <li><a href="#Horizontal-Menu-CSS">Обмен</a></li>
                    <li><a href="#Horizontal-Menu-CSS">Доставка</a></li>
                    <li><a href="#Horizontal-Menu-CSS">Заказать</a></li>
                    <li><a href="">Есть вопросы? 8 (800) 700 43 43</a></li>
                </ul>
            </center>
        </div>  -->
        <!-- end menu block -->

        <!-- start top-left block -->
        <div id="top-left">
            <img src="/img/om1.jpg" width="350px" height="380px" class="firstimg">
            <a href="#openModal1"><img src="/img/om2.png" width="60px" height="60px" class="secondimg"></a>
                <a href="#openModal2"><img src="/img/om3.jpg" width="60px" height="60px" class="thirdimg"></a>
                    <a href="#openModal3"><img src="/img/om4.png" width="60px" height="60px" class="fourthimg"></a>
                        <a href="#openModal4"><img src="/img/om5.JPG" width="60px" height="60px" class="fifthimg"></a>
                            <a href="#openModal5"><img src="/img/om6.jpg" width="60px" height="60px" class="sexthimg"></a>
            <div class="modalDialog1" id="openModal1">
                <div><a class="close" href="#close" title="Закрыть">X</a>
                    <center><img src="/img/om2.png" height="400px" class="secondimg"></center>
                </div>
            </div>
            <div class="modalDialog2" id="openModal2">
                <div><a class="close" href="#close" title="Закрыть">X</a>
                    <center><img src="/img/om3.jpg" height="400px" class="thirdimg"></center>
                </div>
            </div>
            <div class="modalDialog3" id="openModal3">
                <div><a class="close" href="#close" title="Закрыть">X</a>
                    <center><img src="/img/om4.png" height="400px" class="fourthimg"></center>
                </div>
            </div>
            <div class="modalDialog4" id="openModal4">
                <div><a class="close" href="#close" title="Закрыть">X</a>
                    <center><img src="/img/om5.JPG" height="400px" class="fifthimg"></center>
                </div>
            </div>
            <div class="modalDialog5" id="openModal5">
                <div><a class="close" href="#close" title="Закрыть">X</a>
                    <center><img src="/img/om6.jpg" height="400px" class="sexthimg"></center>
                </div>
            </div>
        </div>

        <!-- end top-left block -->

        <!-- start top-center block -->
        <div id="top-center">
            <h1>Omlook X5 pro</h1>
                        <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small"
                             data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus" data-yashareTheme="counter">
                        </div>

            <strong>Основные свойства:</strong><br><br>
            <table>
                <tr>
                    <td><span class="option-word1">Тип:</span></td>
                    <td><span class="option-word2">Смартфон</span></td>
                </tr>
                <tr>
                    <td><span class="option-word1">Страна-изготовитель:</span></td>
                    <td><span class="option-word2">Китай</span></td>
                </tr>
                <tr>
                    <td><span class="option-word1">Год выпуска:</span></td>
                    <td><span class="option-word2">2015</span></td>
                </tr>
                <tr>
                    <td><span class="option-word1">Модель:</span></td>
                    <td><span class="option-word2">x5 pro</span></td>
                </tr>
                <tr>
                    <td><span class="option-word1">Диагональ:</span></td>
                    <td><span class="option-word2">5.0"</span></td>
                </tr>
                <tr>
                    <td><span class="option-word1">Цвет:</span></td>
                    <td><span class="option-word2"> <img src="/img/black-color.png" style="max-width: 18px; border: 1px double black;" title="Чёрный">
                            <img src="/img/white-color.png" style="max-width: 18px; border: 1px double black;" title="Белый"> <img src="/img/red-color.png" style="max-width: 18px; border: 1px double black;" title="Красный">
                            <img src="/img/green-color.png" style="max-width: 18px; border: 1px double black;" title="Зелёный"> <img src="/img/yelow-color.png" style="max-width: 18px; border: 1px double black;" title="Жёлтый">
                            <img src="/img/blue-color.png" style="max-width: 18px; border: 1px double black;" title="Синий"> <img src="/img/pink-color.png" style="max-width: 18px; border: 1px double black;" title="Розовый"></span></td>
                </tr>
                <tr>
                    <td><u>В наличии!</u></td>
                </tr>

            </table>
            <br><br>
            <div id="order">
                <a href="payment.php"><img src="/img/buy.png" style="max-width: 220px"></a>
                <h2>Цена: <s>7800</s> 6999 рублей</h2>
            </div>

        </div>
        <!-- end top-center block -->

        <!-- start top-right block -->
        <div id="top-right">

                <!--<div id="phone">
                    <p>
                        Гарячая линия:
                         +7 495 135 06 82
                    </p>
                </div>-->

                <!-- start price block -->

            <div id="price">

                    <ul style="font-size: 18px">
                        <li>Гарячая линия: +7 495 135 06 82<br>
                        Пн-Пт, с 09 до 18, <a href="#openModal8">Заказать обратный звонок</a>
                        </li>
                        <div class="modalDialog8" id="openModal8">
                            <div><a class="close" href="#close" title="Закрыть">X</a>
                                <center><br>
                                    Заполните, пожалуйста, форму. <br> Наш менеджер свяжется с Вами в ближейшее время!<br><br>
                                    <form action="callback.php" method="post" name="frm_feedback">
                                        <label>Введите ваш номер:</label>
                                        <input type="text" name="user_telephone1" /><br/><br>
                                        <input type="submit" value="Заказать" name="btn_submit" />
                                    </form>
                                </center>
                            </div>
                        </div>
                        <li><a href="#openModal6" style="color: red">Оформить заказ по телефону!</a></li>
                    </ul>

                <div class="modalDialog6" id="openModal6">
                    <div><a class="close" href="#close" title="Закрыть">X</a>
                        <center><br>
                            Заполните, пожалуйста, форму. <br> Наш менеджер свяжется с Вами в ближейшее время!<br><br>
                            <form action="callbackbuy.php" method="post" name="frm_feedback">
                                <label>Введите ваш номер:</label>
                                <input type="text" name="user_telephone1" /><br/><br>
                                <input type="submit" value="Заказать" name="btn_submit" />
                            </form>
                        </center>
                    </div>
                </div>

                <div class="modalDialog7" id="openModal7">
                    <div><a class="close" href="#close" title="Закрыть">X</a>
                        <center><br>
                            Спасибо! Ваш нормер телефона откравлен. <br>
                            Мы скоро с Вами свяжемся!
                        </center>
                    </div>
                </div>

            <strong>Наши преимущества:</strong><br>
                <ul>
                    <li>Лучшее соотношение цена/качество, по сравнению с известными брендами</li>
                    <li>Обмен телефона на новую модель через год</li>
                    <li>Бесплатная доставка</li>
                    <li>Встроено уникальное мобильное приложение Omlook</li>
                </ul>

                <strong>Индивидуальная надпись на телефон:</strong><br>
                <ul>
                    <li>Устали от шаблонного и стандартного? Добавьте изюминку к привычным вещам!
                        Выберите надпись или рисунок – и мы нанесём их <strong>на корпус Вашего телефона</strong>. Omlook – Выделяйтесь!</li>
                </ul>
            </div>
            <br>
            <!--<div id="price2">

            </div> -->
            <!-- end price block -->

        </div>
        <!-- end top-right block -->

        <!-- start description block -->
        <div id="description">

            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Описание</a></li>
                    <li><a href="#tabs-2">Характеристики</a></li>
                    <li><a href="#tabs-3">Отзывы</a></li>
                    <li><a href="#tabs-4">Гарантия</a></li>
                    <li><a href="#tabs-5">Доставка</a></li>
                    <li><a href="#tabs-8">Оплата</a></li>
                    <li><a href="#tabs-6">Сравнить</a></li>
                    <li><a href="#tabs-7">Индивидуальная надпись</a></li>
                </ul>
                <div id="tabs-1">
                    <center><strong><span class="title">Внешний вид</span></strong></center>
                    <br>

                    <p>
                        Наиболее интересной особенностью данного бюджетного смартфона является его цветовое разнообразие.
                        В продажу гаджет поступит в 6 цветах: традиционном белом и черном, а также в мягких пастельных оттенках
                        голубого, розового, зеленого и желтого.
                    </p>
                    <center><img src="/img/om3.jpg" width="315px"></center>
                    <br>
                    <p>
                        Производители также заявили о том, что задняя панель телефона будет обладать специальным УФ-покрытием,
                        которое сделает пластик очень мягким и приятным на ощупь.
                    </p>
                    <br>
                    <center><strong><span class="title">Процессор, видеоускоритель</span></strong></center>
                    <br>
                    <p>
                        Устройство оснащено процессором MT6735 с 4 вычислительными ядрами, тактовая частота которых 1.3 ГГц.
                        Открывайте без проблем любое приложение и пользуйтесь им без потери уровня быстродействия системы.
                        За качество изображения отвечает графический процессор ARM Mali-T720.
                    </p>
                    <center><img src="/img/om6.jpg" width="515px"></center>
                    <br>
                    <center><strong><span class="title">Экран</span></strong></center>
                    <br>
                    <p>
                        Смартфон имеет 5-дюймовый экран с IPS матрицей, которая может отображать до 16 миллионов цветов. Дисплей
                        с HD качеством и разрешением экрана 1280х720 пикселей позволит вам наслаждаться отличным изображением и графикой.
                    </p>
                    <br>
                    <center><strong><span class="title">Память</span></strong></center>
                    <br>
                    <p>
                        Несмотря на первоначальную информацию об 1 Гб ОЗУ и 8 Гб постоянной памяти, недавно производители
                        подтвердили, что они увеличат данный объем в 2 раза. Таким образом, мы получим смартфон с 2 Гб
                        оперативной и с 16 Гб внутренней памяти. О наличии возможности подключения карты памяти пока не сообщается.
                    </p>
                </div>
                <div id="tabs-2">
                    <table rules="rows">
                        <tr>
                            <td>
                                <strong><span class="title">ОБЩИЕ ХАРАКТЕРИСТИКИ:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ТИП</span></td>
                            <td><span class="option-word2">смартфон</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ОПЕРАЦИОННАЯ СИСТЕМА	</span></td>
                            <td><span class="option-word2">Android 5.1</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ТИП КОРПУСА	</span></td>
                            <td><span class="option-word2">классический</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">МАТЕРИАЛ КОРПУСА</span></td>
                            <td><span class="option-word2">пластик</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">УПРАВЛЕНИЕ</span></td>
                            <td><span class="option-word2">сенсорные кнопки</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ТИП SIM-КАРТЫ</span></td>
                            <td><span class="option-word2">micro SIM</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">КОЛИЧЕСТВО SIM-КАРТ</span></td>
                            <td><span class="option-word2">2</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">РЕЖИМ РАБОТЫ НЕСКОЛЬКИХ SIM-КАРТ</span></td>
                            <td><span class="option-word2">попеременный</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ВЕС</span></td>
                            <td><span class="option-word2">130 г</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">РАЗМЕРЫ (ШXВXТ)</span></td>
                            <td><span class="option-word2">72.2x143x8.8 мм</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">ЭКРАН:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ТИП ЭКРАНА</span></td>
                            <td><span class="option-word2">цветной IPS, 16.78 млн цветов, сенсорный</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ТИП СЕНСОРНОГО ЭКРАНА</span></td>
                            <td><span class="option-word2">мультитач, емкостный</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ДИАГОНАЛЬ</span></td>
                            <td><span class="option-word2">5 дюйм.</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">РАЗМЕР ИЗОБРАЖЕНИЯ</span></td>
                            <td><span class="option-word2">1280x720</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ЧИСЛО ПИКСЕЛЕЙ НА ДЮЙМ (PPI)	</span></td>
                            <td><span class="option-word2">294</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">МУЛЬТИМЕДИЙНЫЕ ВОЗМОЖНОСТИ:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ФОТОКАМЕРА</span></td>
                            <td><span class="option-word2">5 млн пикс., светодиодная вспышка</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ЗАПИСЬ ВИДЕОРОЛИКОВ	</span></td>
                            <td><span class="option-word2">есть</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ФРОНТАЛЬНАЯ КАМЕРА	</span></td>
                            <td><span class="option-word2">есть, 2 млн пикс.</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">АУДИО</span></td>
                            <td><span class="option-word2">MP3, FM-радио</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">РАЗЪЕМ ДЛЯ НАУШНИКОВ	</span></td>
                            <td><span class="option-word2">3.5 мм</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">СВЯЗЬ:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">СТАНДАРТ</span></td>
                            <td><span class="option-word2">GSM 900/1800/1900, 3G, 4G</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ИНТЕРФЕЙСЫ</span></td>
                            <td><span class="option-word2">Wi-Fi 802.11n, Bluetooth 4.0, USB</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">СПУТНИКОВАЯ НАВИГАЦИЯ</span></td>
                            <td><span class="option-word2">GPS</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">CИСТЕМА A-GPS	</span></td>
                            <td><span class="option-word2">есть</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">ПАМЯТЬ И ПРОЦЕССОР:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ПРОЦЕССОР</span></td>
                            <td><span class="option-word2">MediaTek MT6735, 1000 МГц</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">КОЛИЧЕСТВО ЯДЕР ПРОЦЕССОРА</span></td>
                            <td><span class="option-word2">4</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ВИДЕОПРОЦЕССОР</span></td>
                            <td><span class="option-word2">Mali-T720</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ОБЪЕМ ВСТРОЕННОЙ ПАМЯТИ	</span></td>
                            <td><span class="option-word2">16 Гб</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ОБЪЕМ ОПЕРАТИВНОЙ ПАМЯТИ	</span></td>
                            <td><span class="option-word2">2 Гб</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">СЛОТ ДЛЯ КАРТ ПАМЯТИ	</span></td>
                            <td><span class="option-word2">есть, объемом до 32 Гб</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">ПИТАНИЕ:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">ЕМКОСТЬ АККУМУЛЯТОРА	</span></td>
                            <td><span class="option-word2">2400 мАч</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">ДРУГИЕ ФУНКЦИИ:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">УПРАВЛЕНИЕ</span></td>
                            <td><span class="option-word2">голосовой набор, голосовое управление</span></td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">РЕЖИМ ПОЛЕТА	</span></td>
                            <td><span class="option-word2">есть</span></td>
                        </tr>
                        <tr>
                            <td><br>
                                <strong><span class="title">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="option-word1">КОМПЛЕКТАЦИЯ</span></td>
                            <td><span class="option-word2">смартфон, батарея, зарядное устройство, кабель USB</span></td>
                        </tr>
                    </table>
                </div>
                <div id="tabs-3">
                    <!-- start vk-comments -->
                    <center>
                        <div id="vk_comments"></div>
                        <script type="text/javascript">
                            VK.Widgets.Comments("vk_comments", {limit: 10, width: "900", attach: "*"});
                        </script>
                    </center>
                    <!-- end vk-comments -->
                </div>
                <div id="tabs-4">
                    <p>Гарантия на телефон – 1 год, и, благодаря работе собственного сервисного центра Omlook в
                        Москве, права покупателя максимально защищены, а качество обслуживания – на порядок выше.<br><br>
                        Адрес сервисного центра в Москве: Смольная, дом 35, корпус 1, телефон +7(499) 397-71-56
                        <br><br>Более того, через год старый телефон можно обменять на новую модель, что, несомненно,
                        является очень привлекательным бонусом.</p>
                </div>
                <div id="tabs-5">
                    <p><strong>Способ доставки:</strong><br>
                        1. <u>Самовывоз</u> в Москве. Адрес пункта выдачи: 119334, Российская Федерация, г. Москва, Большая Тульская, дом 10, строение 5, офис 536,
                           тел.: +7 495 135 06 82, график работы пн-пт с 09 до 18.<br>
                        2. <u>Ruston Express</u> (http://www.ruston.cc/ru/index.php). Стоимость доставки: бесплатно.
                        <br><br>
                        <strong>Регионы доставки:</strong><br>
                        Мы обеспечиваем доставку телефона в Россию, Украину, Беларусь и Казахстан.
                        <br><br>
                        <strong>Срок доставки:</strong> <br>
                        14-35 дней, в зависимости от региона.

                    </p>
                </div>
                <div id="tabs-6">
                    <center><strong><span class="title">Цена</span></strong></center>
                    <p><center>Мы сравнили Omlook x5 pro с другими похожими по характеристикам телефонами.</center>

                    <center><img src="/img/5.png" width="800px"></center>
                    <br>
                    <center><strong><span class="title">Omlook x5 pro VS Samsung Galaxy Grand Neo Plus GT-I9060I/DS</span></strong></center>
                    <center>
                        <table rules="rows">
                            <tr>
                                <td></td>
                                <td class="omlook"><img src="/img/om3.jpg" width="100px"><br>Omlook x5 pro</td>
                                <td class="other"><img src="/img/Samsung.jpg" width="100px"><br>Samsung Galaxy Grand Neo Plus GT-I9060I/DS</td>
                            </tr>
                            <tr>
                                <td class="name">Стоимость</td>
                                <td class="omlook">6999</td>
                                <td class="other">9150</td>
                            </tr>
                            <tr>
                                <td class="name">Операционная система</td>
                                <td class="omlook">Android 5.1</td>
                                <td class="other">Android 4.4</td>
                            </tr>
                            <tr>
                                <td class="name">Память (Гб)</td>
                                <td class="omlook">16</td>
                                <td class="other">8</td>
                            </tr>
                            <tr>
                                <td class="name">Диагональ экрана</td>
                                <td class="omlook">5</td>
                                <td class="other">4.8</td>
                            </tr>
                            <tr>
                                <td class="name">Разрешение экрана</td>
                                <td class="omlook">1280x720 (HD)</td>
                                <td class="other">800x480</td>
                            </tr>
                            <tr>
                                <td class="name">Объем оперативной памяти (Мб)</td>
                                <td class="omlook">2048</td>
                                <td class="other">1024</td>
                            </tr>
                        </table>
                    </center>
                    <br>
                    <center><strong><span class="title">Omlook x5 pro VS Sony Xperia M2 Dual sim (D2302)</span></strong></center>
                    <center>
                        <table rules="rows">
                            <tr>
                                <td></td>
                                <td class="omlook"><img src="/img/om3.jpg" width="100px"><br>Omlook x5 pro</td>
                                <td class="other"><img src="/img/Sony.jpg" width="100px"><br>Sony Xperia M2 Dual sim (D2302)</td>
                            </tr>
                            <tr>
                                <td class="name">Стоимость</td>
                                <td class="omlook">6999</td>
                                <td class="other">12380</td>
                            </tr>
                            <tr>
                                <td class="name">Операционная система</td>
                                <td class="omlook">Android 5.1</td>
                                <td class="other">Android 4.3</td>
                            </tr>
                            <tr>
                                <td class="name">Память (Гб) ?</td>
                                <td class="omlook">16</td>
                                <td class="other">8</td>
                            </tr>
                            <tr>
                                <td class="name">Диагональ экрана</td>
                                <td class="omlook">5</td>
                                <td class="other">4.8</td>
                            </tr>
                            <tr>
                                <td class="name">Разрешение экрана</td>
                                <td class="omlook">1280x720 (HD)</td>
                                <td class="other">960x540</td>
                            </tr>
                            <tr>
                                <td class="name">Объем оперативной памяти (Мб)</td>
                                <td class="omlook">2048</td>
                                <td class="other">1024</td>
                            </tr>
                        </table>
                    </center>
                    <br>
                    <center><strong><span class="title">Omlook x5 pro VS HTC Desire 600 Dual Sim</span></strong></center>
                    <center>
                        <table rules="rows">
                            <tr>
                                <td></td>
                                <td class="omlook"><img src="/img/om3.jpg" width="100px"><br>Omlook x5 pro</td>
                                <td class="other"><img src="/img/HTC.jpg" width="100px"><br>HTC Desire 600 Dual Sim</td>
                            </tr>
                            <tr>
                                <td class="name">Стоимость</td>
                                <td class="omlook">6999</td>
                                <td class="other">9750</td>
                            </tr>
                            <tr>
                                <td class="name">Операционная система</td>
                                <td class="omlook">Android 5.1</td>
                                <td class="other">Android 4.1</td>
                            </tr>
                            <tr>
                                <td class="name">Память (Гб) ?</td>
                                <td class="omlook">16</td>
                                <td class="other">8</td>
                            </tr>
                            <tr>
                                <td class="name">Диагональ экрана</td>
                                <td class="omlook">5</td>
                                <td class="other">4.5</td>
                            </tr>
                            <tr>
                                <td class="name">Разрешение экрана</td>
                                <td class="omlook">1280x720 (HD)</td>
                                <td class="other">960x540</td>
                            </tr>
                            <tr>
                                <td class="name">Объем оперативной памяти (Мб)</td>
                                <td class="omlook">2048</td>
                                <td class="other">1024</td>
                            </tr>
                        </table>
                    </center>
                    <br>
                    <center><strong><span class="title">Omlook x5 pro VS Huawei Ascend G500 Pro</span></strong></center>
                    <center>
                        <table rules="rows">
                            <tr>
                                <td></td>
                                <td class="omlook"><img src="/img/om3.jpg" width="100px"><br>Omlook x5 pro</td>
                                <td class="other"><img src="/img/Huawei.jpg" width="100px"><br>Huawei Ascend G500 Pro</td>
                            </tr>
                            <tr class="qwert">
                                <td class="name">Стоимость</td>
                                <td class="omlook">6999</td>
                                <td class="other">9700</td>
                            </tr>
                            <tr>
                                <td class="name">Операционная система</td>
                                <td class="omlook">Android 5.1</td>
                                <td class="other">Android 4</td>
                            </tr>
                            <tr>
                                <td class="name">Память (Гб) ?</td>
                                <td class="omlook">16</td>
                                <td class="other">4</td>
                            </tr>
                            <tr>
                                <td class="name">Диагональ экрана</td>
                                <td class="omlook">5</td>
                                <td class="other">4.3</td>
                            </tr>
                            <tr>
                                <td class="name">Разрешение экрана</td>
                                <td class="omlook">1280x720 (HD)</td>
                                <td class="other">960x540</td>
                            </tr>
                            <tr>
                                <td class="name">Объем оперативной памяти (Мб)</td>
                                <td class="omlook">2048</td>
                                <td class="other">1024</td>
                            </tr>
                            <tr>
                                <td class="name">Количество ядер процессора</td>
                                <td class="omlook">4</td>
                                <td class="other">2</td>
                            </tr>
                        </table>
                    </center>


                    </p>

                </div>
                <div id="tabs-7">
                    <p><strong>Индивидуальная надпись на телефон:</strong><br>
                        Все мы любим выделяться. Нам нравится, когда наш внешний вид и вещи, которыми мы пользуемся,
                        отражают наш внутренний мир. Для нас важны наши имена, нам не чужда любовь к приятным мелочам,
                        вроде картинки или фразы, которые каким-то образом связаны с нами.
                        <br>
                        <center><img src="/img/your-logo1.jpg" style="max-height: 500px"></center>
                        <br>
                        Не поэтому ли люди делают татуировки, а до татуировок часто использовалась гравировка на часах
                        и ювелирных изделиях, вышивка на носовых платках, шарфах… Такие важные мелочи.
                        <br>
                        <center><img src="/img/your-logo2.jpg" style="max-height: 500px"></center>
                        <br>
                        Телефон современного человека – тоже отражение его личности, наравне с одеждой, причёской.
                        Нам хочется, чтобы телефон, сделанный на конвейере и похожий на множество других, имел
                        какую-то изюминку. Именно поэтому процветает индустрия аксессуаров для мобильных устройств.
                        <br><br>
                        А мы подумали: что если вместо стандартной надписи с названием модели и производителя
                        телефона наносить индивидуальные надписи и рисунки на заказ? То есть Вы как бы получаете
                        возможность участвовать в дизайне своего телефона. Хотите ли Вы видеть на нём своё имя,
                        номер телефона, любимую фразу – вроде Carpe Diem – глядя на которую вдохновляетесь или
                        которая напоминает о важном, картинку, забавную или серьёзную, – это только Ваш выбор.
                        <br><br>
                        Данная опция полностью бесплатна!
                        <br>
                        Мы ждём Вас и Ваши пожелания!

                    </p>
                </div>
                <div id="tabs-8">
                    <p><strong>Способы оплаты:</strong><br>
                    <ul>
                        <li>Карты Visa/MasterCard</li><br>
                    </ul>

                    </p>
                </div>

            </div>

        </div>
        <!-- end description block -->

        <!-- start fotter block -->
    <div id="fotter">
        <table cellpadding="15px">
            <tr>
                <td>
                    <img src="/img/visamc.gif" style="max-height: 60px">
                </td>
                <td>
                    <h5>
                        Omlook Ltd., Office 11, 43 Bedford street, Covent Garden  London UK<br>
                        Регистрационный номер 7647188,
                        тел.: +7 495 135 06 82; e-mail: w@omlook.com<br>
                        <a href="/Site rules.pdf?1" target="_blank">Правила и условия использования сайта</a> |
                        <a href="/Exchange and Returns.pdf?1" target="_blank">Обмен и возврат</a>
                    </h5>
                </td>
                <td>
                    <h5>
                        ООО «Омлоок», ОГРН: 1117746611234<br>
                        Юридический адрес: 191991, Российская Федерация, г. Москва, Ленинский пр-т, дом 63/2,корпус 1<br>
                        <a href="/Privacy policy.pdf?1" target="_blank">Политика конфиденциальности</a>
                    </h5>
                </td>
            </tr>
        </table>
    </div>
        <!-- end fotter block -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter33145493 = new Ya.Metrika({
                        id:33145493,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/33145493" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</div>
<!-- end container block -->

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'bogZ8Xhqfy';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->

</body>

</html>